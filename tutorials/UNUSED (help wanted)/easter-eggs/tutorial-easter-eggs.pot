msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2009-05-24 11:40+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: easter-eggs-f13.svg:44(format) easter-eggs-f12.svg:44(format) easter-eggs-f11.svg:44(format) easter-eggs-f10.svg:44(format) easter-eggs-f09.svg:44(format) easter-eggs-f08.svg:44(format) easter-eggs-f07.svg:44(format) easter-eggs-f06.svg:44(format) easter-eggs-f05.svg:44(format) easter-eggs-f04.svg:44(format) easter-eggs-f03.svg:44(format) easter-eggs-f02.svg:44(format) easter-eggs-f01.svg:44(format) 
msgid "image/svg+xml"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:24(None) 
msgid "@@image: 'easter-eggs-f01.svg'; md5=533640baaf821717dbd4fff4d7af1386"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:35(None) 
msgid "@@image: 'easter-eggs-f02.svg'; md5=8b0ba516bd6144f447792430930e08c9"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:46(None) 
msgid "@@image: 'easter-eggs-f03.svg'; md5=5a38534f4d800140ecd1b435e43cc30e"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:57(None) 
msgid "@@image: 'easter-eggs-f04.svg'; md5=3622b6ec62e44129de17479810628cf9"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:73(None) 
msgid "@@image: 'easter-eggs-f05.svg'; md5=6d831b891d409b8ae94729b0e9df84de"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:85(None) 
msgid "@@image: 'easter-eggs-f06.svg'; md5=bd6db6c45ad35f45f45a4094a89967ef"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:99(None) 
msgid "@@image: 'easter-eggs-f07.svg'; md5=fc5e4dbadf86b35ec5bf261bc78e6c64"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:110(None) 
msgid "@@image: 'easter-eggs-f08.svg'; md5=c4c18c4752462754b72f5aa47122bb64"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:121(None) 
msgid "@@image: 'easter-eggs-f09.svg'; md5=6c49da108b4969e3e6a4804886189b8f"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:133(None) 
msgid "@@image: 'easter-eggs-f10.svg'; md5=4f5dd30646531f1515abd1174a854f78"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:147(None) 
msgid "@@image: 'easter-eggs-f11.svg'; md5=f2aa5c31f7f8984ad1ca8ba7b840d36c"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:160(None) 
msgid "@@image: 'easter-eggs-f12.svg'; md5=777eba2a7a9249dbd0f8f4e9fb3d4bd3"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-easter-eggs.xml:173(None) 
msgid "@@image: 'easter-eggs-f13.svg'; md5=3acd13b727b1626fee9da9d6199f0ebe"
msgstr ""

#: tutorial-easter-eggs.xml:4(title) 
msgid "Easter Eggs"
msgstr ""

#: tutorial-easter-eggs.xml:8(para) 
msgid "This tutorial covers the creation of Easter Eggs — basic elliptical shapes that are formed using bezier lines."
msgstr ""

#: tutorial-easter-eggs.xml:15(title) 
msgid "Creating the Egg"
msgstr ""

#: tutorial-easter-eggs.xml:17(para) 
msgid "To make the creation of the egg points easier, turn on the grid and snap. Then create the basic egg shape using the bezier pencil."
msgstr ""

#: tutorial-easter-eggs.xml:29(para) 
msgid "Use the node selection tool, and make the nodes smooth."
msgstr ""

#: tutorial-easter-eggs.xml:40(para) 
msgid "The egg looks a little too fat. Use the node tool and extend the bottom and top node a bit."
msgstr ""

#: tutorial-easter-eggs.xml:51(para) 
msgid "Color our egg, and fatten the outline using the Fill and Stroke tool."
msgstr ""

#: tutorial-easter-eggs.xml:65(title) 
msgid "Adding stripes to the Egg"
msgstr ""

#: tutorial-easter-eggs.xml:67(para) 
msgid "Start with the Rectangle tool and draw a rectangle across the egg."
msgstr ""

#: tutorial-easter-eggs.xml:78(para) 
msgid "Select both objects using the <keycap>Shift</keycap> key while selecting. Perform Path Division on the objects."
msgstr ""

#: tutorial-easter-eggs.xml:90(para) 
msgid "Eggs are curved surfaces, so adjust the curve of the band by first inserting a node in the middle of the band. Using the node tool, select nodes on both sides of the band using <keycap>Shift</keycap>, and then use the Insert new nodes into selected segments command."
msgstr ""

#: tutorial-easter-eggs.xml:104(para) 
msgid "Using the node selection tool, make the band angles into smooth curves."
msgstr ""

#: tutorial-easter-eggs.xml:115(para) 
msgid "Color the band a different color by selecting the band, and using the Fill and Stroke options."
msgstr ""

#: tutorial-easter-eggs.xml:126(para) 
msgid "Add some designs to the band. A star will look great on our egg. After copying the stars onto the egg, use the node tool to adjust the tilt of the star to match the curve of the egg."
msgstr ""

#: tutorial-easter-eggs.xml:138(para) 
msgid "An Easter egg by itself is great, but having several eggs in a frame would be even better. Also, having an upright egg is nice, but tilting it a little will give it some character. Having several eggs that are identical is nice, but having a little variety is nicer. Lets copy our egg, and adjust some things. Group together all the objects in each egg when finished."
msgstr ""

#: tutorial-easter-eggs.xml:152(para) 
msgid "After using the handles to adjust the size of the eggs so there is a little variety, use the handles to rotate each egg to its desired position. Raise or lower each egg to get the desired perspective."
msgstr ""

#: tutorial-easter-eggs.xml:165(para) 
msgid "Use the rectangle tool to frame our eggs and add a background. Use the Fill and Stroke tool to adjust the border size and the background color. Lower the background rectangle behind the eggs, and adjust the edges to meet the eggs at the bottom and the sides."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2.
#: tutorial-easter-eggs.xml:0(None) 
msgid "translator-credits"
msgstr ""

