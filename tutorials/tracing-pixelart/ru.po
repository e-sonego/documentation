#
# Alexandre Prokoudine <alexandre.prokoudine@gmail.com>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing Pixel Art\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2017-01-22 19:39+0300\n"
"Last-Translator: Alexandre Prokoudine <alexandre.prokoudine@gmail.com>\n"
"Language-Team: русский <>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Игорь Жигунов <craysy@gmail.com>, 2016\n"
"Александр Прокудин <alexandre.prokoudine@gmail.com>, 2017"

#. (itstool) path: articleinfo/title
#: tutorial-tracing-pixelart.xml:6
msgid "Tracing Pixel Art"
msgstr "Трассировка пиксель-арта"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing-pixelart.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:11
msgid "Before we had access to great vector graphics editing software..."
msgstr "До того как появились прекрасные редакторы векторной графики…"

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:14
msgid "Even before we had 640x480 computer displays..."
msgstr "Даже до дисплеев с разрешением 640x480…"

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:17
msgid ""
"It was common to play video games with carefully crafted pixels in low "
"resolutions displays."
msgstr ""
"Совершенно привычным делом было играть в видеоигры с тщательно "
"прорисованными пикселями на дисплеях с низким разрешением."

#. (itstool) path: abstract/para
#: tutorial-tracing-pixelart.xml:20
msgid "We name \"Pixel Art\" the kind of art born in this age."
msgstr "Этот вид искусста мы называем \"Pixel Art\"."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:24
msgid ""
"Inkscape is powered by <ulink url=\"https://launchpad.net/libdepixelize"
"\">libdepixelize</ulink> with the ability to automatically vectorize these "
"\"special\" Pixel Art images. You can try other types of input images too, "
"but be warned: The result won't be equally good and it is a better idea to "
"use the other Inkscape tracer, potrace."
msgstr ""
"Inkscape использует <ulink url=\"https://launchpad.net/libdepixelize"
"\">libdepixelize</ulink> — библиотеку автоматической векторизации "
"изображений в стиле пиксель-арт. Вы можете использовать этот инструмент и "
"для обычной растровой графики, но результат не будет так же хорош. Для "
"прочих изображений мы рекомендуем обычный векторизатор в Inkscape на основе "
"Potrace."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:30
msgid ""
"Let's start with a sample image to show you the capabilities of this tracer "
"engine. Below there is an example of a raster image (taken from a Liberated "
"Pixel Cup entry) on the left and its vectorized output on the right."
msgstr ""
"Давайте начнём с образца изображения, чтобы показать вам возможности этого "
"движка векторизации. Ниже приводится пример растрового изображения (взято из "
"Liberated Pixel Cup) слева и его векторизованного вывода справа."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:41
msgid ""
"libdepixelize uses Kopf-Lischinski algorithm to vectorize images. This "
"algorithm uses ideas of several computer science techniques and math "
"concepts to produce a good result for pixel art images. One thing to notice "
"is that the alpha channel is completely ignored by the algorithm. "
"libdepixelize has currently no extensions to give a first-class citizen "
"treatment for this class of images, but all pixel art images with alpha "
"channel support are producing results similar to the main class of images "
"recognized by Kopf-Lischinski."
msgstr ""
"libdepixelize использует алгоритм Kopf-Lischinski для векторизации "
"изображений. Этот алгоритм использует идеи из нескольких методов "
"компьютерной науки и математических понятий для получения хорошего "
"результата из пиксель-арта. Заметьте, что алгоритм полностью игнорирует "
"альфа-канал. libdepixelize не имеет в настоящее время расширения, для "
"восстановления данного класса изображений, но все изображения в стиле "
"пиксель-арт с поддержкой альфа-канала дают результаты, аналогичные основному "
"классу образов, распознаваемых алгоритмом Kopf-Lischinski."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:55
msgid ""
"The image above has alpha channel and the result is just fine. Still, if you "
"find a pixel art image with a bad result and you believe that the reason is "
"the alpha channel, then contact libdepixelize maintainer (e.g. fill a bug on "
"the project page) and he will be happy to extend the algorithm. He can't "
"extend the algorithm if he don't know what images are giving bad results."
msgstr ""
"Изображение выше имеет альфа-канал, и получившийся результат просто "
"отличный. Тем не менее, если вы найдете изображение в стиле пиксель-арт с "
"плохим результатом, и вы уверены, что причина в альфа-канале, напишите об "
"этом создателю libdepixelize (например, чтобы заполнить ошибку на странице "
"проекта), и он будет рад улучшить алгоритм. Если автор проекта не будет "
"знать об ошибках, улучшить алгоритм он не сможет."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:61
#, fuzzy
msgid ""
"The image below is a screenshot of <guimenuitem>Pixel art</guimenuitem> "
"dialog in the English localisation. You can open this dialog using the "
"<menuchoice><guimenu>Path</guimenu><guisubmenu>Trace Bitmap</"
"guisubmenu><guimenuitem>Pixel art</guimenuitem></menuchoice> menu or right-"
"clicking on an image object and then <guimenuitem>Trace Bitmap</guimenuitem>."
msgstr ""
"Изображение ниже — скриншот диалога  <command>Векторизация пиксельной "
"графики</command> в английской локализации. Вы можете открыть этот диалог, "
"используя меню <command>Контур > Векторизовать пиксельную графику...</"
"command>  или нажав правой кнопкой мыши на изображение объекта, а затем "
"выбрав пункт <command>Векторизация пиксельной графики</command> в "
"открывшемся меню."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:74
msgid ""
"This dialog has two sections: Heuristics and output. Heuristics is targeted "
"at advanced uses, but there already good defaults and you shouldn't worry "
"about that, so let's leave it for later and starting with the explanation "
"for output."
msgstr ""
"Это диалоговое окно состоит из двух разделов: эвристика и вывод. Эвристика "
"ориентирована на продвинутых пользователей, но настройки по умолчанию уже "
"хороши, и вам не нужно об этом беспокоиться, так что давайте оставим это на "
"потом и начнём с объяснения вывода."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:79
msgid ""
"Kopf-Lischinski algorithm works (from a high-level point of view) like a "
"compiler, converting the data among several types of representation. At each "
"step the algorithm has the opportunity to explore the operations that this "
"representation offers. Some of these intermediate representations have a "
"correct visual representation (like the reshaped cell graph Voronoi output) "
"and some don't (like the similarity graph). During development of "
"libdepixelize users kept asking for adding the possibility of export these "
"intermediate stages to the libdepixelize and the original libdepixelize "
"author granted their wishes."
msgstr ""
"Алгоритм Kopf-Lischinski работает (с точки зрения высокого уровня) как "
"компилятор, конвертируя данные из нескольких типов представления. На каждом "
"шаге алгоритм имеет возможность изучить операции, которые это представление "
"предлагает. Некоторые из этих промежуточных представлений имеют правильное "
"визуальное представление (как реконструированный клеточный граф вывода "
"алгоритма Вороного), а некоторые — нет (как графы подобия). Во время "
"разработки libdepixelize пользователи просили добавить возможность экспорта "
"промежуточных этапов, и разработчик согласился внести предложенное новшество."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:87
#, fuzzy
msgid ""
"The default output should give the smoothest result and is probably what you "
"want. You saw already the default output on the first samples of this "
"tutorial. If you want to try it yourself, just open the <guimenuitem>Trace "
"Bitmap</guimenuitem> dialog, select <guimenuitem>Pixel art</guimenuitem> tab "
"and click in <guibutton>OK</guibutton> after choosing some image on Inkscape."
msgstr ""
"Вывод по умолчанию должен дать смягченный результат — возможно тот, который "
"вы хотите. Вы уже видели результат вывода по умолчанию на первых образцах "
"этого урока. Если вы хотите испробовать этот способ самостоятельно, просто "
"откройте диалог трассировки <command>Векторизация пиксельной графики</"
"command> и нажмите кнопку <command>OK</command> после выбора какого-либо "
"изображения в Inkscape."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:93
msgid ""
"You can see the Voronoi output below and this is a \"reshaped pixel image\", "
"where the cells (previously pixels) got reshaped to connect pixels that are "
"part of the same feature. No curves will be created and the image continues "
"to be composed of straight lines. The difference can be observed when you "
"magnify the image. Previously pixels couldn't share a edge with a diagonal "
"neighbour, even if it was meant to be part of the same feature. But now "
"(thanks to a color similarity graph and the heuristics that you can tune to "
"achieve a better result), it's possible to make two diagonal cells share an "
"edge (previously only single vertices were shared by two diagonal cells)."
msgstr ""
"Вы можете увидеть вывод алгоритма Вороного ниже — это «реформированное "
"пиксельное изображение», где клетки (ранее пиксели) были изменены, чтобы "
"содеинить части одного объекта. Кривые при этом не создаются, и изображение "
"по-прежнему состоит из прямых линий. Разница будет видна при увеличении "
"изображения. Ранее пиксели не могли разделять край с соседом по диагонали. "
"Но теперь (благодаря графу цветового подобия и настраиваемой эвристике, "
"которые дают лучший результат) возможно заставить две диагональных клетки "
"разделить край (ранее разделялись лишь единичные вершины)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:109
msgid ""
"The standard B-splines output will give you smooth results, because the "
"previous Voronoi output will be converted to quadratic Bézier curves. "
"However, the conversion won't be 1:1 because there are more heuristics "
"working to decide which curves will be merged into one when the algorithm "
"reaches a T-junction among the visible colors. A hint about the heuristics "
"of this stage: You can't tune them."
msgstr ""
"Стандартный B-Spline вывод даст вам гладкие результаты, потому что "
"предыдущий вывод Вороного будет преобразован в квадратичные кривые Безье. "
"Тем не менее, преобразование не будет один к одному, потому что там больше "
"эвристик, решающих когда кривые будут объединены в одну при достижении "
"алгоритмом Т-образного перекрёстка среди видимых цветов. Подсказываем про "
"эвристики этого этапа: их пока нельзя настроить."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:115
msgid ""
"The final stage of libdepixelize (currently not exportable by the Inkscape "
"GUI because of its experimental and incomplete state) is \"optimize curves\" "
"to remove the staircasing effect of the B-Spline curves. This stage also "
"performs a border detection technique to prevent some features from being "
"smoothed and a triangulation technique to fix the position of the nodes "
"after optimization. You should be able to individually disable each of these "
"features when this output leaves the \"experimental stage\" in libdepixelize "
"(hopefully soon)."
msgstr ""
"Заключительный этап libdepixelize (в настоящее время не экспортируется в "
"интерфейсе Inkscape из-за своей недоработанности) — это оптимизация кривых "
"для удаления ступенчатого эффекта кривых B-Spline. На этом этапе также "
"выполняется обнаружение границ, чтобы предотвратить некоторые особенности "
"сглаживания, и применяется триангуляция, чтобы исправить положение узлов "
"после оптимизации. Вы сможете индивидуально отключать каждую из этих "
"функций, когда этот вывод перестанет считаться экспериментальным в "
"libdepixelize (надеемся, скоро)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:122
msgid ""
"The heuristics section in the gui allows you to tune the heuristics used by "
"libdepixelize to decide what to do when it encounters a 2x2 pixel block "
"where the two diagonals have similar colors. \"What connection should I keep?"
"\" is what libdepixelize asks. It tries to apply all heuristics to the "
"conflicting diagonals and keeps the connection of the winner. If a tie "
"happens, both connections are erased."
msgstr ""
"Раздел эвристики в интерфейсе позволяет настраивать эвристики, используемые "
"libdepixelize для решения, что делать, когда он сталкивается с пиксельным "
"блоком размером 2x2, где две диагонали имеют схожие цвета. «Какое соединение "
"я должен сохранить?» — вот что решает libdepixelize. Он пытается применить "
"все эвристики для конфликтующих диагоналей и сохраняет соединение "
"«победителя». Если происходит связывание, оба соединения удаляются."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:128
msgid ""
"If you want to analyze the effect of each heuristic and play with the "
"numbers, the best output is the Voronoi output. You can see more easily the "
"effects of the heuristics in the Voronoi output and when you are satisfied "
"with the settings you got, you can just change the output type to the one "
"you want."
msgstr ""
"Если вы хотите проанализировать влияние каждого правила и поиграть с "
"числами, лучший вывод — вам лучше выбрать вывод Вороного. Вы можете легче "
"увидеть эффекты эвристики на выводе Вороного и когда вы будете удовлетворены "
"полученными настройками, можете просто изменить тип вывода на нужный вам."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:133
msgid ""
"The image below has an image and the B-Splines output with only one of the "
"heuristics turned on for each try. Pay attention to the purple circles that "
"highlight the differences that each heuristic performs."
msgstr ""
"Изображения ниже построчно состоят из оригинального изображения и B-Splines "
"вывода с единственной включенной эвристикой. Обратите внимание на фиолетовые "
"круги, которые указывают на различия в выполнении эвристик."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:144
msgid ""
"For the first try (top image), we only enable the curves heuristic. This "
"heuristic tries to keep long curves connected together. You can notice that "
"its result is similar to the last image, where the sparse pixels heuristic "
"is applied. One difference is that its \"strength\" is more fair and it only "
"gives a high value to its vote when it's really important to keep these "
"connections. The \"fair\" definition/concept here is based on \"human "
"intuition\" given the pixel database analysed. Another difference is that "
"this heuristic can't decide what to do when the connections group large "
"blocks instead of long curves (think about a chess board)."
msgstr ""
"Для первой попытки (верхнее изображение) включена только эвристика кривых. "
"Эта эвристика пытается сохранить длинные кривые, связанные вместе. Можно "
"заметить, что её результат аналогичен последнему изображению, в котором "
"применена эвристика разрежённых пикселей. Одно из отличий состоит в том, что "
"«сила» этой эвристики более справедлива и применима, когда действительно "
"важно сохранить связи. Определение «справедливости» основано на "
"«человеческой интуиции», применяемой к пикселю из базы проанализированных "
"данных. Ещё одно отличие в том, что эта эвристика не может решить, что "
"делать, когда группа соединений состоит из больших блоков, а не длинных "
"кривых (представьте себе шахматную доску)."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:152
msgid ""
"For the second try (the middle image), we only enable the islands heuristic. "
"The only thing this heuristic does is trying to keep the connection that "
"otherwise would result in several isolated pixels (islands) with a constant "
"weight vote. This kind of situation is not as common as the kind of "
"situation handled by the other heuristics, but this heuristic is cool and "
"help to give still better results."
msgstr ""
"Для второй попытки (среднее изображение) включена только островная "
"эвристика. Единственное, что делает эта эвристика — пытается удержать связь, "
"которая в противном случае приводит к нескольким изолированным пикселям "
"(островам) с постоянным «весом голосования». Подобная ситуация встречается "
"не так часто, поскольку такие ситуации обрабатываются другими эвристиками. "
"Но этот эвристический подход в таких случаях даёт ещё лучшие результаты."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:158
msgid ""
"For the third try (the bottom image), we only enable the sparse pixels "
"heuristic. This heuristic tries to keep the curves with the foreground color "
"connected. To find out what is the foreground color the heuristic analyzes a "
"window with the pixels around the conflicting curves. For this heuristic, "
"you not only tune its \"strength\", but also the window of pixels it "
"analyzes. But keep in mind that when you increase the window of pixels "
"analyzed the maximum \"strength\" for its vote will increase too and you "
"might want to adjust the multiplier for its vote. The original libdepixelize "
"author think this heuristic is too greedy and likes to use the \"0.25\" "
"value for its multiplier."
msgstr ""
"Для третьей попытки (нижнее изображение) включена только эвристика "
"разрежённых пикселей. Эта эвристика пытается сохранить кривые, связанные с "
"основным цветом. Для определения, что это цвет переднего плана, эвристика "
"анализирует окно с пикселами вокруг конфликтующих кривых. Для этой эвристики "
"вы настраиваете не только её «силу», но также и размер окна анализируемых ею "
"пикселей. Но имейте в виду, что при увеличении окна анализируемых пикселей "
"максимальная «сила» для «голоса» этого окна также будет увеличиваться, и, "
"возможно, вам придётся настроить множитель для голосования. Автор "
"оригинальной версии libdepixelize думает, что эта эвристика слишком жадная и "
"предпочитает использовать значение \"0,25\" для её множителя."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:167
msgid ""
"Even if the results of the curves heuristic and the sparse pixels heuristic "
"give similar results, you may want to leave both enabled, because the curves "
"heuristic may give an extra safety that the important curves of contour "
"pixels won't be hampered and there are cases that can be only answered by "
"the sparse pixels heuristic."
msgstr ""
"Даже если эвристика кривых и эвристика разрежённых пикселей дают сходные "
"результаты, вы можете оставить их обе включенными, эвристика кривых может "
"дать дополнительную безопасность, чтобы важные кривые контуров пикселей не "
"мешали в тех случаях, которые могут быть разрешены только эвристикой "
"разрежённых пикселей."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:172
msgid ""
"Hint: You can disable all heuristics by setting its multiplier/weight values "
"to zero. You can make any heuristic act against its principles using "
"negative values for its multiplier/weight values. Why would you ever want to "
"replace behaviour that was created to give better quality by the opposite "
"behaviour? Because you can... because you might want a \"artistic\" "
"result... whatever... you just can."
msgstr ""
"Подсказка: вы можете отключить все эвристики, установив значения множителя/"
"веса равным нулю. Вы можете испортить любую эвристику с помощью "
"отрицательных значений для множителя/веса. Почему бы вам, при желании, не "
"заменить настроенное поведение эвристики противоположным поведением? Потому "
"что вы можете… Потому что вы хотите «художественный» результат… Да и "
"неважно… Просто вам так захотелось."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:178
msgid ""
"And that's it! For this initial release of libdepixelize these are all the "
"options you got. But if the research of the original libdepixelize author "
"and its creative mentor succeeds, you may receive extra options that broaden "
"even yet the range of images for which libdepixelize gives a good result. "
"Wish them luck."
msgstr ""
"Вот и всё! В первом выпуске libdepixelize есть пока вот эти возможности. А "
"если исследование автора оригинальной версии libdepixelize и его творческого "
"наставника окажется успешным, вы сможете получить дополнительные функции, "
"которые расширят диапазон изображений, для которых libdepixelize даёт "
"хороший результат. Пожелаем им удачи."

#. (itstool) path: article/para
#: tutorial-tracing-pixelart.xml:183
msgid ""
"All images used here were taken from Liberated Pixel Cup to avoid copyright "
"problems. The links are:"
msgstr ""
"Все изображения, используемые здесь, были взяты из Liberated Pixel Cup, "
"чтобы избежать проблем с авторскими правами. Ссылки ниже:"

#. (itstool) path: listitem/para
#: tutorial-tracing-pixelart.xml:188
msgid ""
"<ulink url=\"http://opengameart.org/content/memento\">http://opengameart.org/"
"content/memento</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-tracing-pixelart.xml:193
msgid ""
"<ulink url=\"http://opengameart.org/content/rpg-enemies-bathroom-tiles"
"\">http://opengameart.org/content/rpg-enemies-bathroom-tiles</ulink>"
msgstr ""

#. (itstool) path: Work/format
#: tracing-pixelart-f01.svg:49 tracing-pixelart-f02.svg:49
#: tracing-pixelart-f03.svg:49 tracing-pixelart-f04.svg:49
#: tracing-pixelart-f05.svg:49
msgid "image/svg+xml"
msgstr ""
